import 'dart:math';
import 'dart:convert';

import 'package:kenken/kenken.dart';

class Kenken {
  Kenken(this.size)
      : cells = List.generate(size, (_) => List.generate(size, (_) => Cell())),
        random = Random(DateTime.now().microsecondsSinceEpoch);

  /// The size of kenken. F.e. size=3 means that kenken has 3 rows and 3 columns.
  int size;
  Random random;

  Iterable<int> get rowOrColNumbers sync* {
    var i = 1;
    while (i <= size) {
      yield i++;
    }
  }

  /// Store for cell values, implemented as `List`(rows) of `List`(columns) of `int`(values).
  var cells = <List<Cell>>[];

  var initState = <Step>[];

  /// Set all values to null and
  /// fill values from initState.
  void resetToInitState() {
    setAllValuesToNull();
    initState.forEach((step) => applyStep(step));
  }

  void applyStep(Step step) {
    setValue(step.rc.row, step.rc.col, step.value, recordIt: false);
  }

  var history = <Step>[];

  /// Is recording on?
  var recording = false;

  /// Custom the kenken rules
  var customRules = <Rule>[];

  /// Returns all kenken rules;
  List<Rule> get rules => generalRules.cast<Rule>() + customRules.cast<Rule>();

  /// Adds a one custom rule to kenken.
  /// Use this method to manually (by your own program logic) add custom rules to kenken.
  /// See also `notCoveredByCustomRulesCells` method.
  /// Sample:
  /// ```
  /// kenken.addRuleFor([RC(1, 2), RC(1, 3)], Sum(, label: '+'), 5)
  /// ```
  void addRuleFor(
      List<RC> cellsCoordinates, Operation operation, validOperationResult) {
    var rule = Rule(cellsCoordinates, operation, validOperationResult);
    addRule(rule);
  }

  void addRule(Rule rule) {
    if (rule.cellsCoordinates.length == 1) {
      // when rule has single cell:
      var r = rule.cellsCoordinates[0].row;
      var c = rule.cellsCoordinates[0].col;
      setValue(r, c, rule.validOperationResult, recordIt: false);
      initState.add(Step(RC(r, c), rule.validOperationResult));
    }
    customRules.add(rule);
    cellOf(rule.coordinatesToDisplayTheRule).doesDisplayRule = true;
    for (var rc in rule.cellsCoordinates) {
      cellOf(rc).itRule = rule;
      cellOf(rc).isTopBorderBold = !rule.hasRC(RC(rc.row - 1, rc.col));
      cellOf(rc).isBottomBorderBold = !rule.hasRC(RC(rc.row + 1, rc.col));
      cellOf(rc).isLeftBorderBold = !rule.hasRC(RC(rc.row, rc.col - 1));
      cellOf(rc).isRightBorderBold = !rule.hasRC(RC(rc.row, rc.col + 1));
    }
  }

  /// Generates common (general) kenken rules.
  /// These rules are general rules for all kenkens.
  /// They require from kenken unique numbers in columns and rows.
  /// Initially, kenken always contains general rules.
  /// The programmer does not have to worry about adding general rules to kenken.
  List<Rule> get generalRules {
    var rules = List.generate(size, (i) {
      var rowNumber = i + 1;
      var rowCoordinates = coordinatesOfRow(rowNumber);
      return Rule(
          rowCoordinates, Operation(isUniq, label: 'uniqueness in row'), true);
    });
    var colRules = List.generate(size, (i) {
      var colNumber = i + 1;
      var colCoordinates = coordinatesOfCol(colNumber);
      return Rule(
          colCoordinates, Operation(isUniq, label: 'uniqueness in col'), true);
    });
    rules.addAll(colRules);
    return rules;
  }

  /// Is custom rules cover full kenken?
  /// Returns list of cells not covered by custom rules.
  /// If the list is empty, then kenken fully covered by custom rules.
  /// The programmer can create his own logic for filling the kenken with custom rules.
  /// With this method, he can control the completeness of the custom rules.
  List<RC> get notCoveredByCustomRulesCells {
    // prepare coverage filled with `false` for each cell:
    var rulesCoverage = List.generate(size, (_) => List.filled(size, false));
    // now set in `true` each cell for each rule:
    for (var rule in customRules) {
      for (var cellCoordinale in rule.cellsCoordinates) {
        rulesCoverage[cellCoordinale.row - 1][cellCoordinale.col - 1] = true;
      }
    }
    // if all cells in `rulesCoverage` are `true`,
    // then kenken full-covered by rules
    var notCoveredCells = <RC>[];
    for (var rowInd = 0; rowInd < rulesCoverage.length; rowInd++) {
      for (var colInd = 0; colInd < rulesCoverage.length; colInd++) {
        if (rulesCoverage[rowInd][colInd] == false) {
          notCoveredCells.add(RC(rowInd + 1, colInd + 1));
        }
      }
    }
    return notCoveredCells;
  }

  /// Use it for set cell `value` by cell coordinates `x` and `y`.
  /// Coordinates should be in `1...kenken.size` diapazone.
  /// If recordIt==true - then will record it to history.
  /// Else if recordIt == null and recording==true - then will record it to history.
  void setValue(int row, int col, int value, {bool recordIt}) {
    var errMsg = _coordErrorMessage(row, col);
    if (errMsg.isNotEmpty) {
      throw ArgumentError(errMsg);
    }
    (cells[row - 1])[col - 1].value = value;
    if (recordIt == true || (recordIt == null && recording)) {
      history.add(Step(RC(row, col), value));
    }
  }

  /// Returns list of coordinates for row with given `rowNumber`
  List<RC> coordinatesOfRow(int rowNumber) =>
      List.generate(size, (i) => RC(rowNumber, i + 1));

  /// Returns list of coordinates for column with given `colNumber`
  List<RC> coordinatesOfCol(int colNumber) =>
      List.generate(size, (i) => RC(i + 1, colNumber));

  /// Returns list of coordinates from `list` of `list (size=2)` of `int`.
  /// First digit is row, second digit is column.
  ///
  /// Sample: `RCList([[2,1],[2,2],[3,2]])`.
  /// ```
  ///       col1 col2 col3
  ///      -------------------
  /// row1 |     |     |     |
  ///      ------|------------
  /// row2 |  x  |  x  |     |
  ///      ------|------------
  /// row3 |     |  x  |     |
  ///      -------------------
  /// ```
  List<RC> RCList(List<List<int>> coordinates) {
    var result = <RC>[];
    for (var rowColumn in coordinates) {
      if (rowColumn.length != 2) {
        throw ArgumentError(
            'each [row,col] should have length=2. But: ${rowColumn}');
      }
      if (rowColumn
          .where((rowOrColumn) => rowOrColumn < 1 || rowOrColumn > size)
          .isNotEmpty) {
        throw ArgumentError('Bad row or column: ${rowColumn}');
      }
      result.add(RC(rowColumn[0], rowColumn[1]));
    }
    return result;
  }

  String _coordErrorMessage(int x, int y) {
    if (x < 1 || x > size || y < 1 || y > size) {
      return 'Bad coordinates: $x,$y';
    }
    return '';
  }

  /// Returns cell value by it coordinates
  int valueOf(RC cellCoord) => cellOf(cellCoord).value;

  /// Returns values of all cells of given coordinates list
  List<int> valuesOf(List<RC> coordinatesList) =>
      coordinatesList.map((rc) => valueOf(rc)).toList();

  /// Return Cell object by it  coordinates
  Cell cellOf(RC cellCoord) {
    var errMsg = _coordErrorMessage(cellCoord.row, cellCoord.col);
    if (errMsg.isNotEmpty) {
      throw ArgumentError(errMsg);
    }
    return cells[cellCoord.row - 1][cellCoord.col - 1];
  }

  /// Returns broken rules for kenken.
  /// If brokenRules list is empty, then kenken are solved.
  /// Any rule is valid (not broken)
  /// if actual result after applying rule predicate to cells values is equal
  /// valid operation result, defined in rule.
  List<Rule> get brokenRules => rules
      .where((rule) =>
          rule.operation.predicate(valuesOf(rule.cellsCoordinates)) !=
          rule.validOperationResult)
      .toList();

  /// Returns a list of consecutive integers from 1 to size of kenken
  List<int> get rowOrColNumbersList => rowOrColNumbers.toList();

  /// Returns random index from 0 to `(to-1)` or `(size-1)` by default
  int randomIndex({int to}) => random.nextInt(to ?? size);

  /// Returns random element for given array
  T randomElement<T>(List<T> list) => list[randomIndex(to: list.length)];

  /// Returns RC with random coordinates within kenken.
  RC get randomRC => RC(randomIndex() + 1, randomIndex() + 1);

  /// Return new list, rotated on `steps` steps to left direction.
  /// `steps` should be 0 or positive integer.
  List<T> rotatedList<T>(List<T> list, int steps) {
    if (steps < 0) {
      throw ArgumentError('steps should be >0');
    }
    if (steps == 0) {
      return list;
    } else {
      return rotatedList(list.sublist(1) + [list[0]], steps - 1);
    }
  }

  /// Returns List of random rotated List of Cells.
  /// It may be used for auto fill kenken with values.
  List<List<int>> get autoGeneratedValues {
    var result = <List<int>>[];
    var initialRow = rowOrColNumbersList..shuffle(random);
    var stepsToRotate = rowOrColNumbersList.map((e) => e - 1).toList()
      ..shuffle();
    for (var shift in stepsToRotate) {
      var row = rotatedList(initialRow, shift);
      result.add(row);
    }
    return result;
  }

  /// Finds coordinates near coordinates with `row`,`col` without rule in it.
  /// If `except` present, then excepts this coordinates from search.
  RC rcNearWithoutRule(int row, int col, {List<RC> except}) {
    var coordinatesNear = [
      RC(row, col + 1),
      RC(row + 1, col),
      RC(row, col - 1),
      RC(row - 1, col),
    ];

    coordinatesNear.shuffle(random);

    for (var rcNear in coordinatesNear ) {
      if (except != null && except.isContainsRC(rcNear)) {
        continue;
      }
      if (rcNear.inBounds(size) && cellOf(rcNear).itRule == null) {
        return rcNear;
      }
    }
    return null;
  }

  /// Seeks `wantedSize` rules-free coordinates after `rs`.
  /// Returns List of RC with size from 1 to `wantedSize`
  /// depending on the success of the search.
  List<RC> tryExpandRC(RC rc, int wantedSize) {
    ArgumentError.checkNotNull(wantedSize);
    var result = <RC>[rc];
    while (result.length < wantedSize) {
      var lastRC = result.last;
      var cellNear = rcNearWithoutRule(lastRC.row, lastRC.col, except: result);
      if (cellNear == null) {
        break;
      }
      result.add(cellNear);
    }
    return result;
  }

  /// Returnsmax rule size for given operation
  int maxSizeForOperation(Operation op) {
    var maxes = <Function, int>{
      sum: size,
      diff: 2,
      mul: size,
      div: 2,
    };
    return maxes[op.predicate];
  }

  /// Set values of each cell of kenken to null.
  void setAllValuesToNull() {
    for (var r in rowOrColNumbers) {
      for (var c in rowOrColNumbers) {
        setValue(r, c, null, recordIt: false);
      }
    }
  }

  /// Clears all rules and all values.
  void clear() {
    cells = List.generate(size, (_) => List.generate(size, (_) => Cell()));
    customRules = <Rule>[];
  }

  /// Automatic fill (refill) the kenken with rules.
  /// After that kenken is ready to solve by user.
  /// Default `useOperations` are: [Div(),Diff(),Sum(),Mul(),],
  /// But you can limit this list to simplify kenken.
  ///
  /// `maxRuleLength` - you can set the maximum length
  ///  of the group of cells that the rule applies to (sum and multiplication).
  /// maximum=kenken.size, default=kenken.size~/2+1.
  ///
  /// `maxRuleResult` - you can limit max digit, which required by rule (in case multiplication).
  /// default=100.
  ///
  /// `singeValueCellsCount` - you can set count of cells with single values. Default = null.
  void autoFillCustomRules(
      {List<Operation> useOperations,
      int maxRuleLength,
      int maxRuleResult = 100,
      int singeValueCellsCount}) {
    clear();
    var values = autoGeneratedValues;
    var operations = useOperations ??
        [
          Div(),
          Diff(),
          Sum(),
          Mul(),
        ];

    for (var r in rowOrColNumbers) {
      for (var c in rowOrColNumbers) {
        setValue(r, c, values[r - 1][c - 1], recordIt: false);
      }
    }

    if (singeValueCellsCount != null && singeValueCellsCount>0) {
      for (var _ in Iterable<int>.generate(singeValueCellsCount)) {
        var rc = randomElement(notCoveredByCustomRulesCells);
        var rcList = tryExpandRC(rc, 1);
        var operation = SingleValue();
        var validOperationResult = operation.predicate([valueOf(rc)]);
        var rule = Rule(rcList, operation, validOperationResult);

        addRule(rule);
      }
    }

    while (notCoveredByCustomRulesCells.isNotEmpty) {
      var startRC = randomElement(notCoveredByCustomRulesCells);

      var operation = randomElement(operations);
      var wantedSize = maxSizeForOperation(operation);
      maxRuleLength = maxRuleLength ?? size ~/ 2 + 1;
      maxRuleLength = min(maxRuleLength, size);
      if (maxRuleLength != null) {
        wantedSize = min(maxRuleLength, wantedSize);
      }
      ArgumentError.checkNotNull(wantedSize);
      var rcList = tryExpandRC(startRC, wantedSize);
      var rcListValues = rcList.map((rc) => valueOf(rc)).toList();
      var applicable =
          operation.isApplicable(rcListValues, maxResult: maxRuleResult);
      if (!applicable) {
        operation = operations.where((o) => o.isApplicable(rcListValues)).first;
      }
      var validOperationResult = operation.predicate(rcListValues);
      var rule = Rule(rcList, operation, validOperationResult);

      addRule(rule);
    }

    for (var r in rowOrColNumbers) {
      for (var c in rowOrColNumbers) {
        if (cellOf(RC(r, c)).itRule.cellsCoordinates.length == 1) {
          continue;
        }
        setValue(r, c, null, recordIt: false);
      }
    }
  }

  @override
  String toString() {
    // return '$cells';
    var result = [];
    for (var r in rowOrColNumbers) {
      for (var c in rowOrColNumbers) {
        var cell = cellOf(RC(r, c));
        result.add((c == 1 ? '\n' : '') + '($r,$c) $cell');
      }
    }
    return '$result';
  }

  Map<String, dynamic> toJson() => {
        'size': size,
        'cells': jsonEncode(cells),
        'customRules': jsonEncode(customRules),
        'initState': jsonEncode(initState),
        'history': jsonEncode(history),
      };

  Kenken.fromJson(Map<String, dynamic> decodedJson) {
    size = decodedJson['size'];
    ArgumentError.checkNotNull(size);

    random = Random(DateTime.now().microsecondsSinceEpoch);
    cells = List.generate(size, (_) => List.generate(size, (_) => Cell()));

    var _customRules = decodedJson['customRules'];
    ArgumentError.checkNotNull(_customRules);
    var _aa = jsonDecode(_customRules) as List; //<Map<String,dynamic>>;
    var _bb = _aa.cast<Map>().map((e) => Rule.fromJson(e)).toList();
    for (var r in _bb) {
      addRule(r);
    }

    var initStateJsonString = decodedJson['initState'];
    ArgumentError.checkNotNull(initStateJsonString);
    var initStateList = jsonDecode(initStateJsonString) as List;
    initState = initStateList.map((s) => Step.fromJson(s)).toList();

    var historyJsonString = decodedJson['history'];
    ArgumentError.checkNotNull(historyJsonString);
    var histroryList = jsonDecode(historyJsonString) as List;
    history = histroryList.map((s) => Step.fromJson(s)).toList();

    var _cells = decodedJson['cells'];
    ArgumentError.checkNotNull(_cells);
    var _cellsL = jsonDecode(_cells);

    cells = (_cellsL as List)
        .map((r) => (r as List).map((c) => Cell.fromJson(c)).toList())
        .toList();

    for (var rule in customRules) {
      for (var rc in rule.cellsCoordinates) {
        cellOf(rc).itRule = rule;
      }
    }
  }
}

/// A rule validates list of cells.
/// List of rules validates a whole kenken.
class Rule<ShouldHaveResult> {
  /// `cellsCoordinates` - list of cells that the rule applies to.
  /// `operation` - what to do with cell in the rule. td: список операций
  /// `validOperationResult` - which result is correct.
  Rule(this.cellsCoordinates, this.operation, this.validOperationResult);

  List<RC> cellsCoordinates;
  Operation operation;
  ShouldHaveResult validOperationResult;

  // Does show label for this operation?
  bool get showLabel {
    if (cellsCoordinates.length == 1) {
      return false;
    }
    if (operation is SingleValue) {
      return false;
    }
    return true;
  }

  @override
  String toString() =>
      '$operation on $cellsCoordinates should be $validOperationResult';

  Map<String, dynamic> toJson() => {
        'cc': cellsCoordinates.map((rc) => rc.toJson()).toList(),
        'op': operation.toJson(),
        'vr': validOperationResult,
      };

  Rule.fromJson(Map<String, dynamic> decodedJson) {
    var op = decodedJson['op'];
    ArgumentError.checkNotNull(op);
    operation = Operation.fromJson(op);

    var coord = decodedJson['cc'] as List;
    ArgumentError.checkNotNull(coord);
    cellsCoordinates = coord.map((e) => RC.fromJson(e as Map)).toList();
    validOperationResult = decodedJson['vr'];
    ArgumentError.checkNotNull(validOperationResult);
  }

  /// Answers the question: is the rule has cell with such coordinates.
  /// ```
  /// rule.hasRC(RC(2, 3));
  /// ```
  bool hasRC(RC cell) {
    return cellsCoordinates
        .where((c) => c.row == cell.row && c.col == cell.col)
        .isNotEmpty;
  }

  /// In kenken, the operation and the expected result are displayed
  /// in the topmost and leftmost cell of each group of cells.
  /// This method returns the coordinates of such a cell.
  RC get coordinatesToDisplayTheRule {
    var topmostCell = cellsCoordinates.reduce((RC prevCell, RC currentCell) =>
        prevCell.row < currentCell.row ? prevCell : currentCell);
    var leftmostCell = cellsCoordinates
        .where((cell) => cell.row == topmostCell.row)
        .reduce((prevCell, currentCell) =>
            prevCell.col < currentCell.col ? prevCell : currentCell);
    return leftmostCell;
  }
}

/// RC coordinates. Coordinates should be in 1...kenken.size diapazone.
class RC {
  RC(this.row, this.col);
  int row;
  int col;

  @override
  String toString() => '($row,$col)';

  Map<String, dynamic> toJson() => {
        'r': row,
        'c': col,
      };

  RC.fromJson(Map<String, dynamic> decodedJson) {
    row = decodedJson['r'];
    col = decodedJson['c'];
  }

  /// Is coordinates are in bounds of kenken with size `size`?
  bool inBounds(int size) => row > 0 && row <= size && col > 0 && col <= size;
}

class Cell {
  Rule itRule;
  bool isTopBorderBold;
  bool isLeftBorderBold;
  bool isRightBorderBold;
  bool isBottomBorderBold;
  bool doesDisplayRule = false;
  int value;

  /// Getter for value as string.
  String get stringValue => value.toString();

  /// Setter for value from string.
  /// Without recording.
  set stringValue(String v) {
    value = int.tryParse(v);
  }

  /// Returns cell value as String or empty string if value is null.
  String get valueAsString => (value ?? '').toString();

  /// Returns rule text (if rule text should display in this cell).
  String get ruleText {
    if (!doesDisplayRule) {
      return '';
    }
    var operLblText = itRule.showLabel ? itRule.operation.label : '';
    return '${itRule.validOperationResult}$operLblText';
  }

  @override
  String toString() {
    if (itRule == null) return 'HAS_NOT_RULE_REFERENCE';
    if (doesDisplayRule == null) return 'DOES_DISPLAY_RULE_IS_NULL';
    return 'Txt:[$ruleText] Val:$value L:$isLeftBorderBold R:$isRightBorderBold T:$isTopBorderBold B:$isBottomBorderBold';
  }

  Map<String, dynamic> toJson() => {
        'v': value,
        'lb': isLeftBorderBold,
        'tb': isTopBorderBold,
        'rb': isRightBorderBold,
        'bb': isBottomBorderBold,
        'dv': doesDisplayRule,
      };

  /// Creates an empty kenken cell element as box for store value and cell properties.
  Cell();

  /// Creates a cell from Map.
  Cell.fromJson(Map<String, dynamic> decodedJson) {
    value = decodedJson['v'];
    isTopBorderBold = decodedJson['tb'];
    isLeftBorderBold = decodedJson['lb'];
    isRightBorderBold = decodedJson['rb'];
    isBottomBorderBold = decodedJson['bb'];
    doesDisplayRule = decodedJson['dv'];
    // itRule - will be set later
  }
}

/// Operation will be apply to list of cells for validate a rule.
/// Operation constructor has `predicate` as positional parameter
/// ( it is function to be apply to cells)
/// `label` - printable operation symbol, accesible by `toString()` method
/// (for display it on kenken or for debug purposes).
/// ```
/// Sum(, label: '+')
/// ```
class Operation {
  Operation(this.predicate, {this.label = ''});
  factory Operation.fromJson(Map<String, dynamic> decodedJson) {
    return operationByName(decodedJson['op']);
  }

  final String label;
  Predicate predicate;

  @override
  String toString() => label;
  Map<String, dynamic> toJson() => {};
  bool isApplicable(List<int> list, {int maxResult}) => true;
}

class Sum extends Operation {
  /// Sums values.
  Sum() : super(sum, label: '+');
  @override
  bool isApplicable(List<int> list, {int maxResult}) => true;
  @override
  Map<String, dynamic> toJson() => {
        'op': 'Sum',
      };
}

class Diff extends Operation {
  /// Subtracts the lower value from the larger
  Diff() : super(diff, label: '-');
  @override
  Map<String, dynamic> toJson() => {
        'op': 'Diff',
      };

  @override
  bool isApplicable(List<int> list, {int maxResult}) => list.length == 2;
}

class Mul extends Operation {
  /// Multiplies values
  Mul() : super(mul, label: '×');

  @override
  Map<String, dynamic> toJson() => {
        'op': 'Mul',
      };

  @override
  bool isApplicable(List<int> list, {int maxResult}) =>
      predicate(list) <= (maxResult ?? 100);
}

class Div extends Operation {
  /// Divides greater value by less
  Div() : super(div, label: '÷');

  @override
  Map<String, dynamic> toJson() => {
        'op': 'Div',
      };

  @override
  bool isApplicable(List<int> list, {int maxResult}) {
    if (list.length != 2) {
      return false;
    }
    for (var el in list) {
      ArgumentError.checkNotNull(el);
      if (el == 0) {
        throw ArgumentError.value('$list (one element is zero)');
      }
    }
    var big = list[0] > list[1] ? list[0] : list[1];
    var small = list[0] > list[1] ? list[1] : list[0];
    if (big.remainder(small) > 0) {
      return false;
    }
    return true;
  }
}

class SingleValue extends Operation {
  /// Represent a single value in a single cell.
  SingleValue() : super(singleValue, label: '');
  @override
  Map<String, dynamic> toJson() => {
        'op': 'SingleValue',
      };

  @override
  bool isApplicable(List<int> list, {int maxResult}) => list.length == 1;
}

var operations = {
  'Sum': Sum(),
  'Diff': Diff(),
  'Mul': Mul(),
  'Div': Div(),
  'SingleValue': SingleValue(),
};

/// Returns operation by it name.
/// A names of operations are:
/// 'Sum','Diff','Mul','Div','SingleValue'
Operation operationByName(String name) {
  ArgumentError.checkNotNull(name);
  var op = operations[name];
  ArgumentError.checkNotNull(op);
  return op;
}

/// Function which can be applyed to List of values
typedef Predicate = Function(List<int>);

/// Is list of values unique?
bool isUniq(List<int> values) => values.isUniq;

/// Returns single value (first element value)
int singleValue(List<int> values) => values[0];

/// Returns sum of elements
int sum(List<int> values) =>
    values.where((e) => e != null).fold(0, (v, e) => v + e);

/// Returns multiplication of elements
int mul(List<int> values) =>
    values.where((e) => e != null).fold(1, (v, e) => v * e);

/// Returns division of mostest element by least
int div(List<int> values) {
  if (values[0] == null ||
      values[1] == null ||
      values[0] == 0 ||
      values[1] == 0) {
    return 0;
  }
  if (values[0] > values[1]) {
    return values[0] ~/ values[1];
  }
  return values[1] ~/ values[0];
}

/// Returns difference of two values (biggest - smallest)
int diff(List<int> values) {
  if (values[0] == null || values[1] == null) {
    return 0;
  }
  if (values[0] > values[1]) {
    return values[0] - values[1];
  }
  return values[1] - values[0];
}

extension ListUniq<T> on List<T> {
  /// Is List values uniq?
  bool get isUniq {
    if (length < 2) return true;
    for (var i = 0; i < length - 1; i++) {
      if (sublist(i + 1).contains(this[i])) return false;
    }
    return true;
  }
}

extension ListContainsRC on List<RC> {
  /// Is `list` contains coordinates `rc`?
  bool isContainsRC(RC rc) {
    for (var el in this) {
      if (el.row == rc.row && el.col == rc.col) {
        return true;
      }
    }
    return false;
  }
}

class Step {
  RC rc;
  int value;
  Step(this.rc, this.value);

  Step.fromJson(Map<String, dynamic> decodedJson) {
    value = decodedJson['v'];

    var rc_json = decodedJson['rc'];
    ArgumentError.checkNotNull(rc_json);
    rc = RC.fromJson(rc_json);
  }

  Map<String, dynamic> toJson() => {
        'v': value,
        'rc': rc.toJson(),
      };
}
