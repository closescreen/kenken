# Kenken

Create kenken with defined size.

Auto fill kenken with rules.

Set kenken values.

Check is kenken solved.

Serialize and deserialize kenken via JSON.
