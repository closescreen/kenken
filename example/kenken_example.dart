// This example uses a terminal and command line,
// but most likely you want
// to draw kenken and to handle custom events in a web application.

// You can see a working example of such an application at http://superkenken.ru

import 'dart:io';
import 'dart:convert';
import 'package:kenken/kenken.dart';

void main() {
  // Cteate kenken:
  var kenken = Kenken(3);

  /// You can use automacic filling kenken with rules:
  kenken.autoFillCustomRules();

  // Manually adding rules (if you have own logic):
  // kenken
  //   ..addRuleFor([RC(1, 1)], SingleValue(), 1)
  //   ..addRuleFor([RC(1, 2), RC(1, 3)], Sum(), 5)
  //   ..addRuleFor([RC(2, 1), RC(3, 1)], Diff(), 1)
  //   ..addRuleFor([RC(2, 2), RC(2, 3)], Sum(), 4)
  //   ..addRuleFor([RC(3, 2), RC(3, 3)], Sum(), 3);

  // kenken.brokenRules.isNotEmpty - will return true when kenken will solved:
  while (kenken.brokenRules.isNotEmpty) {
    // kenken.brokenRules - contains all rules what are broken by user:
    print(kenken.brokenRules);
    printKenken(kenken);
    // User enters a row number of cell:
    p('Enter row:');
    var row = int.tryParse(stdin.readLineSync());
    // User enters a column numer of cell:
    p('Enter col:');
    var col = int.tryParse(stdin.readLineSync());
    // User enters a value for cell:
    p('Enter value:');
    var val = int.tryParse(stdin.readLineSync());
    // Put value to kenken cell:
    kenken.setValue(row, col, val);
  }

  p('DONE!');

  // You can also...
  // ... serialize kenken as JSON:
  var kenkenJsonString = jsonEncode(kenken);

  // ... create kenken object from JSON:
  // ignore: unused_local_variable
  var kenkenFromJson = Kenken.fromJson(jsonDecode(kenkenJsonString));

  // How to encode KenKen to urlEncoded string:
  var urlEncoded = base64UrlEncode(utf8.encode(kenkenJsonString));
  print('urlEncoded: $urlEncoded');

  // And vice versa:
  var decodedJsonString = utf8.decode( base64Decode(urlEncoded) );
  print('decoded: $decodedJsonString');
}

/// Primitive sample of way to print kenken to terminal
void printKenken(Kenken kenken) {
  for (var rowNumber in kenken.rowOrColNumbers) {
    for (var colNumber in kenken.rowOrColNumbers) {
      var cell = kenken.cellOf(RC(rowNumber, colNumber));
      // print top borders
      p(topBorderAsString(cell, 8));
    }
    // print new line:
    print('');

    // print Left borders, rules, Right borders
    for (var colNumber in kenken.rowOrColNumbers) {
      var cell = kenken.cellOf(RC(rowNumber, colNumber));
      //print left border, cell text and right border:
      p(wrapTextWithLeftAndRightBorders(
          '${cell.ruleText} ${cell.valueAsString}', cell, 8));
    }
    // print new line:
    print('');
  }
}

/// Prints `val` to stdout without trailing '\n'
void p(val) {
  stdout.write(val);
}

/// Returns string representation of top cell border.
String topBorderAsString(Cell cell, int cellWidth) =>
    cell.isTopBorderBold ? '-' * cellWidth : '|' + ' ' * (cellWidth - 2) + '|';

/// Returns string with left and right borders and text between they
String wrapTextWithLeftAndRightBorders(String text, Cell cell, int cellWidth) {
  var leftBorder = cell.isLeftBorderBold ? '|' : ' ';
  var rightBorder = cell.isRightBorderBold ? '|' : ' ';
  var spacesCount = cellWidth - text.length - 2;
  return '$leftBorder$text' + (' ' * spacesCount) + rightBorder;
}
